# SupDeVinci Equipe 7

## Récupérer le kubeconfig

REQUIS : ```aws cli``` et ```kubectl```

Pour récupérer le kubeconfig il faut exporter les variables AWS contenu dans ce repo en executant les commandes suivantes : 

```shell
export AWS_ACCESS_KEY_ID="MY_ACCESS_KEY_ID"
```

```shell
export AWS_SECRET_ACCESS_KEY="MY_SECRET_ACCESS_KEY"
```

```shell
export AWS_SESSION_TOKEN="MY_SESSION_TOKEN"
```

Ensuite il suffit de récupérer le kubeconfig avec le cli aws avec la commande suivante :

```shell
aws eks --region eu-west-1 update-kubeconfig --name eks-cluster-equipe-07
```

## Infrastructure 

Vous disposez d'un cluster Kubernetes déployé avec le service managé EKS de AmazonWebServices.
Pour déployer les applications un ArgoCD à été installé sur le cluster, ci dessous la liste des outils installés sur le cluster qui vous seront utiles pour le hackaton : 

    - app-grafana
    - app-loki
    - app-mimir
    - app-tempo
    - app-otel

Vous pouvez modifier ces applications.

Les applications suivantes ne doivent absolument pas être modifié :

    - app-argocd
    - argocd-configs
    - infra-apps
    - infra-nfs

Vous pouvez rajoutez d'autres applications si vous le souhaitez via argoCD ou non.

Il y a deux applications qui n'ont pas été déployées avec ArgoCD qui sont :

    - app-pokeshop
    - app-tracetest

Vous n'avez de toute façon pas besoin de toucher à cces applications.

## Url et identifiants de connexion

Les passwords des applications sont enregistrés dans les variables gitlab.

- argocd :
    - url : a16effc0741e247b3862e283b21c7bb5-2014607f31f85095.elb.eu-west-1.amazonaws.com
    - user : admin
- grafana :
    - url : a36bdc2ea6ee04cccb65be74a7f0d515-8d4eff4dd10b4263.elb.eu-west-1.amazonaws.com
    - user : admin
- pokeshop :
    - url : a7b022ff96368481ba3c0ab9e3be7c9c-2120229952.eu-west-1.elb.amazonaws.com
- tracetest :
    - url :  af69e3e50a047419e9e5bba02d5d3622-e92021f9566111ec.elb.eu-west-1.amazonaws.com:11633

Pour utilisez Pokeshop avec tracetest vous pouvez trouvez les documentations içi :

    - https://github.com/kubeshop/pokeshop/blob/master/docs/overview.md
    - https://tracetest.io/

Pour l'utilisation de TraceTest et de Pokeshop n'hésitez pas à nous contacter.    